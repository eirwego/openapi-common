## Eirwego OpenAPI Common

This project gather OpenAPI models and endpoint implementation examples for various use cases:

- OAuth 2

### Prerequisites

Be sure to have these installed:

- GNU Make
- NodeJS
- Yarn

### Set up

Just run this command:

```
yarn install
```

Note that some of the following `Makefile` targets don't work under Windows at the moment.

### Check your specs

Unless you're a CI bot or you don't use [a decent API editor](https://stoplight.io/studio/), you won't need to check your specs, but if you have to, run this command:

```
make check
```

### Build the doc

Because an OpenAPI YAML file is not that easy to read, you may prefer an HTML file. If this is the case, run this command:

```
make html
```

### Run the mock servers

There are 1 mock server you may want to run with one of the following command:

```
yarn run mock:oauth
```

The mock server will be available on `127.0.0.1` on port `4010`.