.DEFAULT_GOAL := html

.PHONY: check
check: node_modules
	yarn run spectral lint reference/*

.PHONY: clean
clean:
	rm -fr html

.PHONY: distclean
distclean:
	rm -fr node_modules

html: html/oauth.html

html/oauth.html: node_modules
	yarn run redoc-cli bundle -o html/oauth.html reference/oauth.v1.yaml

node_modules:
	yarn install
